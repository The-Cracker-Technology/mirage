rm -rf /opt/ANDRAX/mirage

source /opt/ANDRAX/PYENV/python3/bin/activate

/opt/ANDRAX/PYENV/python3/bin/python3 -m pip install wheel

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Pip install wheel... PASS!"
else
  # houston we have a problem
  exit 1
fi

/opt/ANDRAX/PYENV/python3/bin/python3 -m pip install .

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Pip install local package... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX/bin/mirage
chmod -R 755 /opt/ANDRAX/bin/mirage
